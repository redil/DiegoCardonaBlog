# BLOG

A blog powered by HUGO static site generator

## Preview locally

__macOS__

```shell
# Install hugo binary
brew install hugo
# Run a hot reloading server
hugo server -D
```

## Deployment

You can edit with your editor of choice locally and deploy with:

```shell
hugo
surge -p public/ --domain diego.redil.orgo
```

Or connect a cloud cms like Forestry.io which will commit to your 
repository and trigger a continious delivery pipeline.

## Gitlab CI/CD

Setup SURGE\_LOGIN and SURGE\_TOKEN global variables.


# TODO

Build a Docker image and push to gitlabs image registry for faster builds.
